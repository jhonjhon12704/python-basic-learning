def print_hangman(num_of_tries):
    """prints the picture of hangman according to number of tries
    :param num_of_tries
    :type: int
    :return: print picture"""
    HANGMAN_PHOTOS = {1: "x-------x\n", 2:
   "x-------x\n"
   "|\n"
   "|\n"
   "|\n"
   "|\n"
   "|\n", 3: "picture 3:\n"
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|\n"
    "|\n"
    "|\n", 4:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|       |\n"
    "|\n"
    "|\n", 5: "picture 5:\n"
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|\n"
    "|\n", 6:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|      / \n"
    "|\n", 7:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|      / \ \n"
    "|\n"}
    print(HANGMAN_PHOTOS[num_of_tries])


def main():
    num_of_tries = 1
    print_hangman(num_of_tries)


if __name__ == "__main__":
    main()
