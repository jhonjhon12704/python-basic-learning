def sort_tuple_by_price(list_of_tuples):
    """receives list of tuples with item and a price.
    returns sorted list with price as key
    :param list_of_tuples
    :type: list
    :return: a sorted tuple by function, in reverse
    :rtype: list
    """
    return sorted(list_of_tuples, key=get_price_tuple, reverse=True)


def get_price_tuple(tuple1):
    """ receives as tuple item and return the price field value
    :param tuple1
    :type: tuple
    :return: the float value of the second value in the tuple
    :rtype: float
    """
    return float(tuple1[1])


products = [('milk', '5.5'), ('candy', '2.5'), ('bread', '9.0')]
print(sort_tuple_by_price(products))


