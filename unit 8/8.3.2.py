from datetime import datetime


def main():
    dict = {"first_name": "Mariah", "last_name": "Carey", "birth_date": "27.03.1970", "hobbies": ["Sing", "Compose", "Act"]}
    num = int(input("enter number: \n"))
    if num == 1:
        print(dict["last_name"])
    if num == 2:
        date = dict["birth_date"]
        d_obj = datetime.strptime(date, "%Y-%m-%d")
        print(d_obj.month)
    if num == 3:
        print(len(dict["hobbies"]))
    if num == 4:
        print(dict["hobbies"][-1])
    if num == 5:
        dict["hobbies"].append("Cooking")
    if num ==6:
        date = dict["birth_date"]
        d_obj = datetime.strptime(date, "%Y-%m-%d")
        tuple_d = (d_obj.day, d_obj.month, d_obj.year)
        print(tuple_d)
    if num == 7:
        date = dict["birth_date"]
        d_obj = datetime.strptime(date, "%Y-%m-%d")
        dict["age "] = datetime.today().year - d_obj.year
        print(dict["age "])





if __name__ == "__main__":
    main()
