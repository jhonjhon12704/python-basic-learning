def mult_tuple(tuple1, tuple2):
    """gets all combinations between two tuples
    :param tuple1
    :type tuple
    :param tuple2
    :type: tuple
    :return: list of all tuple combinations
    :rtype: list"""
    tup_list = list()
    for item in tuple1:
        for item2 in tuple2:
            new_tuple = (item, item2)
            new_tuple2 = (item2, item)
            tup_list.append(new_tuple)
            tup_list.append(new_tuple2)
    return tup_list


def main():
    first_tuple = (1, 2)
    second_tuple = (4, 5)
    print(mult_tuple(first_tuple, second_tuple))


if __name__ == "__main__":
    main()

