def inverse_dict(my_dict):
    """inverses the given dictionary and the key are stored in lists for every value
    :param my_dict
    :type: dict
    :return new_dict - > dictionary holding lists
    :rtype: dict """
    new_dict = {}
    for k, v in my_dict.items():
        if v not in new_dict:
            new_dict[v] = []
        new_dict[v].append(k)
    for k in new_dict:
        new_dict[k].sort()
    return new_dict


def main():
    course_dict = {'I': 3, 'love': 3, 'self.py!': 2}
    print(inverse_dict(course_dict))


if __name__ == "__main__":
    main()
