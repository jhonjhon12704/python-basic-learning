def count_chars(my_str):
    """function that recieves string and returns a dictionary with the letters as key and their frequencies as value
    :param my_str
    :type: str
    :return: dict -> dictionary
    :rtype: dict"""
    dict= {}
    for item in my_str:
        if item not in dict and item.isalpha():
            num = my_str.count(item)
            dict[item] = num
    return dict


def main():
    magic_str = "abra cadabra"
    print(count_chars(magic_str))


if __name__ == "__main__":
    main()

