def sort_anagrams(list_of_strings):
    """recives a list with anagrams and it sorts it while putting anagrams next to each other
    :param list_of_strings
    :type:  list
    :return: list with anagrams
    :rtype: list
    """
    new_list = list()
    for item in list_of_strings:
        for i in list_of_strings:
            check = True
            for j in item:
                if j not in i:
                    check = False
            if check:
                if item not in new_list:
                    new_list.append(item)
                if i not in new_list:
                    new_list.append(i)
    return new_list


def main():
    list_of_words = ['deltas', 'retainers', 'desalt', 'pants', 'slated', 'generating', 'ternaries', 'smelters',
                     'termless', 'salted', 'staled', 'greatening', 'lasted', 'resmelts']
    print(sort_anagrams(list_of_words))


if __name__ == "__main__":
    main()



