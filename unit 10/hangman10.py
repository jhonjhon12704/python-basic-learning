def choose_word(file_path, index):
    """ returns the word in index given in file and number of words in file
    :param file_path
    :type: str
    :param index
    :type: int
    :return: tuple with number of words and wanted word
    :rtype: tuple"""
    list_words = []
    count = index - 1
    sp_word = ""
    with open(file_path, 'r') as file:
        while count >= 0:
            file.seek(0)
            output = file.read().split(" ")
            for item in output:
                if count == 0:
                    sp_word = item
                    break
                if item not in list_words:
                    list_words.append(item)
                count -= 1
            if sp_word:
                break
    return sp_word


def check_win(secret_word, old_letters_guessed):
    """check if the letters in the word are in the guessed letter list
    :param secret_word
    :type: str
    :param old_letters_guessed
    :type: list
    :return: true if all chars in word guessed false if not"""
    check = True
    for item in secret_word:
        if item not in old_letters_guessed:
            check = False
    return check


def print_hangman(num_of_tries):
    """prints the picture of hangman according to number of tries
    :param num_of_tries
    :type: int
    :return: print picture"""
    HANGMAN_PHOTOS = {1: "x-------x\n", 2:
   "x-------x\n"
   "|\n"
   "|\n"
   "|\n"
   "|\n"
   "|\n", 3:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|\n"
    "|\n"
    "|\n", 4:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|       |\n"
    "|\n"
    "|\n", 5:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|\n"
    "|\n", 6:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|      / \n"
    "|\n", 7:
    "x-------x\n"
    "|       |\n"
    "|       0\n"
    "|      /|\ \n"
    "|      / \ \n"
    "|\n"}
    print(HANGMAN_PHOTOS[num_of_tries])


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """checks if input can be added to guess list
    :param letter_guessed
    :type: str
    :param old_letters_guessed
    :type: list
    :return: true if valid input and not in list, else print X, the letters that was guessed and returns false
    :rtype bool"""
    if not check_valid_input(letter_guessed, old_letters_guessed):
        lower_list = sorted(old_letters_guessed, key=str.lower)
        print("X")
        print("->".join(lower_list))
        return False
    return True


def check_valid_input(letter_guessed, old_letters_guessed):
    """checks if string is valid input for hangman game -> one key which is an english letter and not guessed yet
        :param letter_guessed
        :type: str
        :param old_letters_guessed
        :type: list
        :return: true if valid input, false if not
        :rtype: bool"""
    if not letter_guessed.isalpha():
        return False
    if len(letter_guessed) != 1:
        return False
    if letter_guessed.lower() in old_letters_guessed:
        return False
    return True


def show_hidden_word(secret_word, old_letters_guessed):
    """function that shows hidden word in hangman with already guessed letters
    :param secret_word
    :type: str
    :param old_letters_guessed
    :type: list
    :return: string as word
    :rtype: str"""
    res_text = ""
    for item in secret_word:
        if item in old_letters_guessed:
            res_text += item +" "
        else:
            res_text += "_ "
    return res_text


def main():
    HANGMAN_ASCII_ART: str = """
        Welcome to the game Hangman
         _    _
        | |  | |
        | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __
        |  __  |/ _' | '_ \ / _' | '_ ' _ \ / _' | '_ \ '
        | |  | | (_| | | | | (_| | | | | | | (_| | | | |
        |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                            __/ |
                           |___/\n
        """
    MAX_TRIES = 6
    print(HANGMAN_ASCII_ART, str(MAX_TRIES))
    secret_word = choose_word("C:\\Users\\jhonj\\OneDrive\\מסמכים\\words.txt", 10)
    num_tries = MAX_TRIES
    print_hangman(7 - num_tries)
    old_letters_guessed = []
    while num_tries > 0:
        print(show_hidden_word(secret_word, old_letters_guessed))
        letter = str(input("guess a letter: "))
        if try_update_letter_guessed(letter, old_letters_guessed):
            old_letters_guessed.append(letter)
            if letter not in secret_word:
                num_tries -= 1
                print_hangman(7 - num_tries)
                if num_tries == 0:
                    print("LOST, the word is", secret_word)
            if check_win(secret_word, old_letters_guessed):
                print("WON! the secret word is", secret_word)
                break


if __name__ == "__main__":
    main()
