temp = str(input("enter temprature: "))
temp = temp.strip().lower()
num = float(temp[:-1])
if temp[-1] == "f":
    if temp.find(".") != -1:
        print(str((num * 5 - 160) / 9) + "C")
    else:
        print(str(int((num * 5 - 160) / 9)) + "C")

elif temp[-1] == "c":
    if temp.find(".") != -1:
        print(str((num * 9 + 160) / 5) + "F")
    else:
        print(str(int((num * 9 + 160) / 5)) + "F")

else:
    print("error")

