def extend_list_x(list_x, list_y):
    """joins two lists together and prints them
    :param list_x
    :type: list
    :param list_y
    :type list
    :return: joined list of two lists
    :rtype: list"""
    new_list = list((*list_y, *list_x))
    print(new_list)
    return new_list


def main():
    x = [4, 5, 6]
    y = [1, 2, 3]
    extend_list_x(x, y)

# extend first list to include second in the start


if __name__ == "__main__":
    main()
