def are_lists_equal(list1, list2):
    """checks if both lists have same values
    :param list1
    :type list
    :param list2
    :type list
    :return: true if they have same value, false if not
    :rtype: bool"""
    if set(list1) == set(list2):
        return True
    return False


def main():
    list1 = [0.6, 1, 2, 3]
    list2 = [3, 2, 0.6, 1]
    list3 = [9, 0, 5, 10.5]
    print(are_lists_equal(list1, list3))


# checks if lists have same values and print result


if __name__ == "__main__":
    main()
