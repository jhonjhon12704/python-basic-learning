from collections import deque


def shift_left(my_list):
    """prints and returns the result of left rotation of a list
    :param my_list
    :type list
    :return: new left rotated list and prints it either
    :rtype: list"""
    rotated_list = deque(my_list)
    rotated_list.rotate(-1)
    print(rotated_list)
    return rotated_list


def main():
    shift_left(["monkey", 1.0, 2])
# call a shifting left function on list

if __name__ == "__main__":
    main()
