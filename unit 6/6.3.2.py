def longest(my_list):
    """returns the longest string in list of strings
    :param my_list
    :type list
    :return: max length string
    :rtype: str"""
    max_len = max(my_list, key=len)
    return max_len


def main():
    list1 = ["111", "234", "2000", "goru", "birthday", "09"]
    print(longest(list1))


# prints longest string in list


if __name__ == "__main__":
    main()
