def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """checks if input can be added to guess list
    :param letter_guessed
    :type str
    :param old_letters_guessed
    :type list
    :return: true if valid input and not in list, else print X, the letters that was guessed and returns false
    :rtype bool"""
    if not check_valid_input(letter_guessed, old_letters_guessed):
        lower_list = sorted(old_letters_guessed, key=str.lower)
        print("X")
        print("->".join(lower_list))
        return False
    return True


def check_valid_input(letter_guessed, old_letters_guessed):
    """checks if string is valid input for hangman game -> one key which is an english letter and not guessed yet
        :param letter_guessed
        :type str
        :param old_letters_guessed
        :type list
        :return: true if valid input, false if not
        :rtype: bool"""
    if not letter_guessed.isalpha():
        return False
    if len(letter_guessed) != 1:
        return False
    if letter_guessed.lower() in old_letters_guessed:
        return False
    return True


def main():
    old_letters = ['a', 'p', 'c', 'f']
    result = try_update_letter_guessed('A', old_letters)
    print(result)
# print the result of updating letter list


if __name__ == "__main__":
    main()
