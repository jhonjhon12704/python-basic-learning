def check_valid_input(letter_guessed, old_letters_guessed):
    """checks if string is valid input for hangman game -> one key which is an english letter and not guessed yet
        :param letter_guessed
        :type str
        :param old_letters_guessed
        :type list
        :return: true if valid input, false if not
        :rtype: bool"""
    if not letter_guessed.isalpha():
        return False
    if len(letter_guessed) != 1:
        return False
    if letter_guessed.lower() in old_letters_guessed:
        return False
    return True


def main():
    old_letters = ['a', 'b', 'c']
    result = check_valid_input('s', old_letters)
    print(result)
# print the result of check_valid_input


if __name__ == "__main__":
    main()
