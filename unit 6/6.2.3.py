def format_list(my_list):
    """prints and returns new list with the organs on even indexes and the last location
    :param my_list
    :type list
    :return: modified string with only the even values with spaces and joints
    :rtype str"""
    result_str = ", ".join(my_list[slice(0, len(my_list)-1, 2)])
    result_str += ", and " + my_list[-1]
    print(result_str)
    return result_str


def main():
    my_list = ["hydrogen", "helium", "lithium", "beryllium", "boron", "magnesium"]
    format_list(my_list)
# call the function to have the list formatted


if __name__ == "__main__":
    main()

