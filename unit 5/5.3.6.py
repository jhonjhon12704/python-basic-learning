def filter_teens(a=13, b=13, c=13):
    return fix_age(a) + fix_age(b) + fix_age(c)


def fix_age(num):
    if 13 <= num <= 14 or 16 < num < 20:
        return 0
    return num


print(filter_teens(2, 1, 15))

