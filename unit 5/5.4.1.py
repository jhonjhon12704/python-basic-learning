def func(num1, num2):
    """this function sums two given numbers
    :param num1 number value
    :param num2 number value
    :type num1 int
    :type num2 int
    :return: sum of num1 and num2
    :rtype: int"""
    return num1+num2


def main():
    result = func(1, 2)
    print(result)
# print the result of function func

if __name__ == "__main__":
    main()


