def is_valid_input(letter_guessed):
    """checks if string is valid input for hangman game -> one key which is an english letter
    :param letter_guessed
    :type str
    :return: true if valid input, false if not
    :rtype: bool"""
    if not letter_guessed.isalpha():
        return False
    if len(letter_guessed) != 1:
        return False
    return True


def main():
    result = is_valid_input("a")
    print(result)
# print the result of is_valid_input

if __name__ == "__main__":
    main()

