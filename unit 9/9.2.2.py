def copy_file_content(source, destination):
    """function that copies data from one file to another
    :param source
    :type: str
    :param destination
    :type: str"""
    with open(source, 'r') as f_source:
        with open(destination, 'w') as f_dest:
            f_dest.write(f_source.read())
    f_source.close()
    f_dest.close()


def main():
    copy_file_content("C:\\Users\\jhonj\\OneDrive\\מסמכים\\vacation.txt",
                      "C:\\Users\\jhonj\\OneDrive\\מסמכים\\work.txt")
    filef = open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\work.txt", "r")
    print(filef.read())


if __name__ == "__main__":
    main()
