def are_files_equal(file1, file2):
    """checks if two files are the same
    :param file1
    :type: file
    :param file2
    :type: file
    :return true if same false if isnt
    :rtype: bool"""
    input_file1 = open(file1, "r")
    input_file2 = open(file2, "r")
    str_file1 = input_file1.read()
    str_file2 = input_file2.read()
    booli = str_file1 == str_file2
    input_file1.close()
    input_file2.close()
    return booli


def main():
    print(are_files_equal("C:\\Users\\jhonj\\OneDrive\\מסמכים\\vacation.txt", "C:\\Users\\jhonj\\OneDrive\\מסמכים\\work.txt"))


if __name__ == "__main__":
    main()



