def who_is_missing(file_name):
    """function that recieves file with numbers 1-n not sorted seperated with joints, that one of the numbers is missing
    this finds the missing number, returns it and writes it to another file
    :param file_name
    :type: str file path
    :return: missing number from file
    :rtype: int"""
    with open(file_name, 'r') as file:
        nums = file.read().split(',')
        nums = [int(num) for num in nums]
        nums_sum = sum(nums)
        n = len(nums) + 1
        expected_sum = (n * (n+1)) // 2
        missing_num = expected_sum - nums_sum
        with open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\found.txt", 'w') as outfile:
            outfile.write(str(missing_num))
        return missing_num


def main():
    print(who_is_missing("C:\\Users\\jhonj\\OneDrive\\מסמכים\\listS.txt"))
    file_f = open("C:\\Users\\jhonj\\OneDrive\\מסמכים\\found.txt", 'r')
    print(file_f.read())
    file_f.close()


if __name__ == "__main__":
    main()
