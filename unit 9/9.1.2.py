from collections import deque


def print_sorted_unique_words(file_path):
    """opens file and print the words sorted with no dups
    :param file_path
    :type: str
    :return: printing list"""

    with open(file_path, 'r') as f:
        words = f.read().split()
    print(sorted(set(words)))
    f.close()


def print_file_lines_reversed(file_path):
    """prints every line in file reversed
    :param file_path
    :type: str
    :return: prints line reversed"""
    words = set()
    with open(file_path, 'r') as f:
        for line in f:
            reversed_line = line.rstrip()[::-1]
            print(reversed_line)


def last_lines(file_path, n):
    """prints last n lines in file:
    :param file_path
    :type: str
    :param n
    :type: int
    ":return: prints last n lines in files"""
    with open(file_path) as f:
        last_n_lines = deque(maxlen=n)

        for line in f:
            last_n_lines.append(line.rstrip('\n'))

        for line in reversed(last_n_lines):
            print(line)


def main():
    path = str(input("enter file path: "))
    task = str(input("enter task: "))
    if task == "sort":
        print_sorted_unique_words(path)
    if task == "rev":
        print_file_lines_reversed(path)
    if task == "last":
        num = int(input("enter number: "))
        last_lines(path, num)


if __name__ == "__main__":
    main()
