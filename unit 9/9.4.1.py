def choose_word(file_path, index):
    """ returns the word in index given in file and number of words in file
    :param file_path
    :type: str
    :param index
    :type: int
    :return: tuple with number of words and wanted word
    :rtype: tuple"""
    list_words = []
    count = index - 1
    sp_word = ""
    with open(file_path, 'r') as file:
        while count >= 0:
            file.seek(0)
            output = file.read().split(" ")
            for item in output:
                if count == 0:
                    sp_word = item
                    break
                if item not in list_words:
                    list_words.append(item)
                count -= 1
            if sp_word:
                break
        file.seek(0)
        output = file.read().split(" ")
        for item in output:
            if item not in list_words:
                list_words.append(item)

    return sp_word


def main():
    print(choose_word("C:\\Users\\jhonj\\OneDrive\\מסמכים\\words.txt", 3))


if __name__ == "__main__":
    main()
