from datetime import datetime


def my_mp3_playlist(file_path):
    """returns the longest song, the most common artist and length in file fo songs as tuple
    :param file_path
    :type: str
    :return: tuple contains data about song file
    :rtype: tuple"""
    count = 0
    max_song = ["", "0:00"]
    count_performer = {}
    with open(file_path, 'r') as file:
        song_data = file.read().split('\n')
        for item in song_data:
            count += 1
            song_tuple = item.split(';')
            if datetime.strptime(str(song_tuple[2]), '%H:%M') > datetime.strptime(str(max_song[1]), '%H:%M'):
                max_song = [song_tuple[0], song_tuple[2]]
            if song_tuple[1] in count_performer:
                count_performer[song_tuple[1]] = count_performer.get(song_tuple[1], 0) + 1
            else:
                count_performer[song_tuple[1]] = 1
    max_performer = max(count_performer.values())
    max_tuple = (max_song[0], count, [k for k, v in count_performer.items() if v == max_performer])
    return max_tuple


def main():
    print(my_mp3_playlist("C:\\Users\\jhonj\\OneDrive\\מסמכים\\songs.txt"))


if __name__ == "__main__":
    main()


