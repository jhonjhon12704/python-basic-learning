def my_mp4_playlist(file_path, new_song):
    """writes the name of new song in existing file on the third spot
    :param file_path
    :type: str
    :param new_song
    :type: str
    :return prints new changed file lines"""
    with open(file_path, 'r+') as file:
        lines = file.readlines()
        file.seek(0)
        song_count = 0
        for i, line in enumerate(lines):
            parts = line.strip().split(';')
            if len(parts) < 3:
                continue
            song_count += 1
            if song_count == 3:
                parts[0] = new_song
                lines[i] = ';'.join(parts) + '\n'
                break
        file.seek(0)
        file.writelines(lines)
        file.truncate()
    with open(file_path, 'r') as f:
        print(f.read())


def main():
    my_mp4_playlist("C:\\Users\\jhonj\\OneDrive\\מסמכים\\songs.txt","call me maybe?")


if __name__ == "__main__":
    main()