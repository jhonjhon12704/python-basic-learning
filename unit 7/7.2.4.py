def seven_boom(end_number):
    """simulizes the game 7 boom, for each number in range between 0 and given number,
     say boom when the number contains 7 or mutiplies by 7
      :param end_number
      :type: int
      :return: a list that contains numbers between 0 and end_number and BOOM for the special cases"""
    seven_b_list = list()
    for num in range(end_number+1):
        if str(num).__contains__("7") or num % 7 == 0:
            seven_b_list.append("BOOM")
        else:
            seven_b_list.append(num)
    return seven_b_list


def main():
    print(seven_boom(17))
# print the seven boom game


if __name__ == "__main__":
    main()
