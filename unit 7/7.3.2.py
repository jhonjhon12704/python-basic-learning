def check_win(secret_word, old_letters_guessed):
    """check if the letters in the word are in the guessed letter list
    :param secret_word
    :type: str
    :param old_letters_guessed
    :type: list
    :return: true if all chars in word guessed false if not"""
    check = True
    for item in secret_word:
        if item not in old_letters_guessed:
            check = False
    return check


def main():
    secret_word = "friends"
    old_letters_guessed = ['m', 'p', 'j', 'i', 's', 'k']
    print(check_win(secret_word, old_letters_guessed))
# print the resul of check_win


if __name__ == "__main__":
    main()



