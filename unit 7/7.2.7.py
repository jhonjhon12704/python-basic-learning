def arrow(my_char, max_length):
    """ prints arrow as my_char with max_length as its center
    :param my_char
    :type: char
    :param max_length
    :type: int
    :return: non"""
    for i in range(max_length):
        print(my_char*i)
    for i in range(max_length, 1, -1):
        print(my_char * i)


def main():
   arrow("*", 5)
# print arrow string


if __name__ == "__main__":
    main()