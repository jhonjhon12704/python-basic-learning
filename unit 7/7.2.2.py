def numbers_letters_count(my_str):
    """returns a list where the first place represents the number of digits in string
    and the second one represents the number of letters
    :param my_str
    :type: str
    :return: a list where the first place represents the number of digits in string
    and the second one represents the number of letters
    :type: list"""
    my_list = [0, 0]
    for item in my_str:
        if str(item).isdigit():
            my_list[0] += 1
        else:
            my_list[1] += 1
    return my_list


def main():
    print(numbers_letters_count("Python 3.6.3"))
# print the result of numbers_letters_count


if __name__ == "__main__":
    main()

