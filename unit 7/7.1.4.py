def squared_numbers(start, stop):
    """returns a list that includes the squared number between two numbers
    :param start
    :type int
    :param stop
    :type int
    :return: list that contains squared numbers
    :rtype list"""
    i=start
    num_list = list()
    while i <= stop:
        num_list.append(i*i)
        i += 1
    return num_list


def main():
    result = squared_numbers(-3, 3)
    print(result)
# print the result of squared_numbers


if __name__ == "__main__":
    main()
