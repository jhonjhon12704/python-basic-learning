def sequence_del(my_str):
    """shorts the sequences in my_str and returns new string without sequences
    :param my_str
    :type: str
    :return: string without sequences
    :rtype: str"""
    exist_item = ""
    new_list = ""
    for item in my_str:
        if item != exist_item:
            new_list += item
            exist_item = item

    return new_list


def main():
    print(sequence_del("ppyyyyythhhhhooonnnnn"))
# print strings without sequences


if __name__ == "__main__":
    main()
