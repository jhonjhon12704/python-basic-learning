def show_hidden_word(secret_word, old_letters_guessed):
    """function that shows hidden word in hangman with already guessed letters
    :param secret_word
    :type: str
    :param old_letters_guessed
    :type: list
    :return: string as word
    :rtype: str"""
    res_text = ""
    for item in secret_word:
        if item in old_letters_guessed:
            res_text += item +" "
        else:
            res_text += "_ "
    return res_text


def main():
    secret_word = "mammals"
    old_letters_guessed = ['s', 'p', 'j', 'i', 'm', 'k']
    print(show_hidden_word(secret_word, old_letters_guessed))
# print hidden word


if __name__ == "__main__":
    main()
