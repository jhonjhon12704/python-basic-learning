def is_greater(my_list, n):
    """returns a list where all the numbers from my_list that is bigger than n
    :param my_list
    :type list
    :param n
    :type int
    :return: list where all the numbers from my_list that is bigger than n
    :rtype list"""
    bigger_list = list()
    for item in my_list:
        if item > n:
            bigger_list.append(item)
    return bigger_list


def main():
    result = is_greater([1, 30, 25, 60, 27, 28], 28)
    print(result)
# print the result of is_greater


if __name__ == "__main__":
    main()


