def list_illegal(shopping_list):
    """finds illegal products less than 3
    or contains things that are not letters
    :param shopping_list
    :type: str
    :return: list with illegal products
    :rtype: list
    """
    new_list = list()
    for item in shopping_list:
        if (len(item) < 3) or not all(char.isalpha() for char in item):
            new_list.append(item)
    return new_list


def deleting_dups(shopping_list):
    """deletes all the duplicates in given list
    :param shopping_list
    :type:list
    :return: the list after deletion
    :rtype: list
    """
    for item in shopping_list:
        for i in range(shopping_list.count(item)):
            shopping_list.remove(item)
    return shopping_list


def main():
    shopping_list = input("Enter list of products to shop: ")
    shopping_list = shopping_list.split(",")

    num = input("Enter action: ")

    while num != "9":

        if num == "1":
            print(shopping_list)
        if num == "2":
            print(len(shopping_list))
        if num == "3":
            product = input("Enter name: ")
            if product in shopping_list:
                print("product in list")
            else:
                print("product not in list")

        if num == "4":
            product = input("Enter name: ")
            print("The product is " + str(shopping_list.count(product)) + " times")

        if num == "5":
            product = input("Enter name: ")
            if product in shopping_list:
                shopping_list.remove(product)
                print(shopping_list)
            else:
                print("The product is not in the list")

        if num == "6":
            product = input("Enter name: ")
            shopping_list.append(product)
            print(shopping_list)

        if num == "7":
            print(list_illegal(shopping_list))

        if num == "8":
            shopping_list = deleting_dups(shopping_list)
            print(shopping_list)

        num = input("Enter action: ")


if __name__ == "__main__":
    main()
